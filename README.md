# Proj0-Hello
-------------
Description: Exercise in git in order to familiarize myself with the correct process of version control. The program
'hello.py' should print the message 'Hello world' with the changes I made to the credentials file.

Author: Brenden Watson
Contact address: brendenw@uoregon.edu
